//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutionsService.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserProductSale
    {
        public int ID { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> Price { get; set; }
        public Nullable<bool> IsSatus { get; set; }
        public Nullable<int> UserId { get; set; }
    }
}
