﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutionsService.Models;
using Dapper;

namespace AutionsService.Controllers
{
    public class AdminController : ApiController
    {
        T1808m_Group4Entities1 db = new T1808m_Group4Entities1();

        public IHttpActionResult GetUser_All(string TextSearch, int IsBackList)
        {
            try
            {
                object response;
                var para = new DynamicParameters();
                para.Add("@TextSech", TextSearch);
                para.Add("@IsBackList", IsBackList);
                List<List<dynamic>> lstObject = new List<List<dynamic>>();
                lstObject = ReturnDynamic("GetUser_All", para);
                response = new { data = lstObject[0] };
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public IHttpActionResult GetCategory_All(string TextSearch)
        {
            try
            {
                object response;
                var para = new DynamicParameters();
                para.Add("@TextSearch", TextSearch);
                List<List<dynamic>> lstObject = new List<List<dynamic>>();
                lstObject = ReturnDynamic("GetCategory_All", para);
                response = new { data = lstObject[0] };
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public IHttpActionResult UpdateUser(tblUsers updateUser)
        {
            object response;
            tblUsers check = db.tblUsers.Where(a => a.ID == updateUser.ID).FirstOrDefault();
            if (check != null)
            {
                check.IsBlackList = updateUser.IsBlackList;
                db.Entry(check).State = System.Data.EntityState.Modified;
                db.SaveChanges();
                response = new { data = 1 };
            }
            else
            {
                response = new { data = 2 };
            }
            return Json(response);
        }
        public IHttpActionResult UpdateCategory(tblCategory updateCategory)
        {
            try
            {
                object response;
                tblCategory check = db.tblCategory.Where(a => a.CategoryCode == updateCategory.CategoryCode && a.ID == updateCategory.ID).FirstOrDefault();
                if (check != null)
                {
                    check.CategoryName = updateCategory.CategoryName;
                    db.Entry(check).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    response = new { data = 1 };
                }
                else
                {
                    response = new { data = 2 };
                }
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public IHttpActionResult AddCategory(tblCategory addCategory)
        {
            try
            {
                object response;
                tblCategory check = db.tblCategory.Where(a => a.CategoryName == addCategory.CategoryName).FirstOrDefault();
                if (check == null)
                {
                    var rowIdentify = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                    tblCategory add = new tblCategory();
                    add.CategoryName = addCategory.CategoryName;
                    add.CategoryCode = rowIdentify;
                    add.IsDelete = false;
                    db.tblCategory.Add(add);
                    db.SaveChanges();
                    response = new { data = 1 };
                }
                else
                {
                    response = new { data = 3 };
                }
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public IHttpActionResult DeleteCategory(int ID)
        {
            try
            {
                object response;
                tblCategory check = db.tblCategory.Where(a => a.ID == ID).FirstOrDefault();
                if (check != null)
                {
                    check.IsDelete = true;
                    db.Entry(check).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    response = new { data = 1 };
                }
                else
                {
                    response = new { data = 2 };
                }
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public List<List<dynamic>> ReturnDynamic(string procedure, DynamicParameters parameters)
        {
            List<List<dynamic>> lstDynamic = new List<List<dynamic>>();
            //var connection = CimsConstant.SQL_CONNECTION() as SqlConnection;
            //SqlConnection sqlConnection = new SqlConnection("Server=192.168.50.149;initial catalog=Jobbox_Dev;user id=sa;password=123@123a");
            SqlConnection sqlConnection = new SqlConnection("Server=210.245.95.62;Database=T1808m_Group4;User Id=sa;Password=z@GH7ytQ;");
            var command = sqlConnection.CreateCommand();
            if (command.Connection.State != ConnectionState.Open)
            {
                command.Connection.Open();
            }
            var grid = command.Connection.QueryMultiple(sql: procedure,
                                  param: parameters,
                                  commandType: CommandType.StoredProcedure);
            while (!grid.IsConsumed)
            {
                lstDynamic.Add(grid.Read().ToList());
            }
            command.Connection.Close();
            return lstDynamic;
        }
    }
}
