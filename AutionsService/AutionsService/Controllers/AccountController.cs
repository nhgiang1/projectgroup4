﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AutionsService.Models;

namespace AutionsService.Controllers
{
    public class AccountController : ApiController
    {
        T1808m_Group4Entities1 db = new T1808m_Group4Entities1();
        public class lstLogin
        {
            public string email { get; set; }
            public string pass { get; set; }
        }
        [HttpPost]
        public IHttpActionResult Loggin(lstLogin userLogin)
        {
            object response;
            tblUsers usercheck = db.tblUsers.Where(a => a.Email == userLogin.email && a.Password == userLogin.pass && a.IsBlackList == false
            && a.IsDelete == false).FirstOrDefault();
            if (usercheck != null)
            {
                response = new { login = true, userId = usercheck.ID, typeUser = usercheck.Type };
                return Json(response);
            }
            else
            {
                response = new { login = false };
                return Json(response);
            }
        }
        public IHttpActionResult getUser(int id)
        {
            object response;
            tblUsers user = db.tblUsers.Where(a => a.ID == id).FirstOrDefault();
            response = user;
            return Json(response);
        }
        [HttpPost]
        public IHttpActionResult SignIn(tblUsers lstUser)
        {
            object lstDK;
            tblUsers usercheck = db.tblUsers.Where(a => a.Email == lstUser.Email && a.IsDelete == false).FirstOrDefault();
            if (usercheck != null)
            {
                lstDK = new { IsDK = false };
            }
            else
            {
                tblUsers add = new tblUsers();
                add.Email = lstUser.Email;
                add.UserName = lstUser.UserName;
                add.Password = lstUser.Password;
                add.UserName = lstUser.UserName;
                add.PhoneNumber = lstUser.PhoneNumber;
                add.FullName = null;
                add.Address = null;
                add.SexCode = null;
                add.Urlimg = null;
                add.Card = null;
                add.UpdateBy = null;
                add.CreateBy = null;
                add.Type = true;
                add.IsDelete = false;
                add.IsBlackList = false;
                db.tblUsers.Add(add);
                db.SaveChanges();
                lstDK = new { IsDK = true };
            }
            return Json(lstDK);
        }
        [HttpPost]
        public IHttpActionResult UpdateAccount(tblUsers lstUser)
        {

            object lstUpdate;
            tblUsers usercheck = db.tblUsers.Where(a => a.ID == lstUser.ID && a.IsDelete == false).FirstOrDefault();
            if (usercheck == null)
            {
                lstUpdate = new { IsDK = false };
            }
            else
            {
                usercheck.Email = lstUser.Email;
                usercheck.UserName = lstUser.UserName;
                usercheck.Password = lstUser.Password;
                usercheck.FullName = lstUser.FullName;
                usercheck.SexCode = lstUser.SexCode;
                usercheck.PhoneNumber = lstUser.PhoneNumber;
                db.Entry(usercheck).State = System.Data.EntityState.Modified;
                db.SaveChanges();
                lstUpdate = new { IsDK = true };
            }
            return Json(lstUpdate);
        }
    }
}
