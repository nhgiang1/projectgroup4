﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.IO;
using System.Web.Http;
using AutionsService.Models;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace AutionsService.Controllers
{
    public class ProductController : ApiController
    {
        T1808m_Group4Entities1 db = new T1808m_Group4Entities1();

        [HttpPost]
        public IHttpActionResult Upload()
        {
            object response;
            var file = HttpContext.Current.Request.Files.Count > 0 ?
        HttpContext.Current.Request.Files[0] : null;

            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(
                    HttpContext.Current.Server.MapPath("~/Upload"),
                    fileName
                );

                file.SaveAs(path);
            }
            var pathFile = file != null ? "Upload/" + file.FileName : null;
            response = new { data = pathFile };
            return Json(response);

        }
        [HttpPost]
        public IHttpActionResult CreateProduct(tblProduct lstProduct)
        {
            try
            {
                var rowIdentify = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");
                object response;
                // Tạo sản phẩm
                tblProduct add = new tblProduct();
                add.NameProduct = lstProduct.NameProduct;
                add.StartDate = lstProduct.StartDate;
                add.StartPrice = lstProduct.StartPrice;
                add.CategoryCode = lstProduct.CategoryCode;
                add.UrlimgProduct = lstProduct.UrlimgProduct;
                add.IsDelete = false;
                add.IsSuccess = false;
                add.EnDate = lstProduct.EnDate;
                add.CodeProduct = rowIdentify;

                add.Description = lstProduct.Description;
                add.CreateBy = lstProduct.CreateBy;
                db.tblProduct.Add(add);
                db.SaveChanges();
                int IiProduct = add.ID;
                tblSale userP = new tblSale();
                userP.UserId = lstProduct.CreateBy;
                userP.ProductId = IiProduct;
                db.tblSale.Add(userP);
                db.SaveChanges();
                response = new { data = 1 };
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        [HttpPost]
        public IHttpActionResult EditProduct(tblProduct lstProduct)
        {
            try
            {
                object response;
                tblProduct check = db.tblProduct.Where(a => a.ID == lstProduct.ID && a.IsDelete == false).FirstOrDefault();
                if (check != null)
                {
                    check.NameProduct = lstProduct.NameProduct;
                    check.StartDate = lstProduct.StartDate;
                    check.StartPrice = lstProduct.StartPrice;
                    check.UrlimgProduct = lstProduct.UrlimgProduct;
                    check.IsDelete = false;
                    check.IsSuccess = false;
                    check.EnDate = lstProduct.EnDate;
                    check.Description = lstProduct.Description;
                    db.Entry(check).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    response = new { data = 1 };
                }
                else
                {
                    response = new { data = 2 };
                }

                return Json(response);
            }
            catch
            {
                return null;
            }
        }

        [HttpPost]
        public IHttpActionResult Aution(int ProductId, int UserId, int Price)
        {
            try
            {
                object response;
                tblUserProduct check = db.tblUserProduct.Where(a => a.ID == ProductId).FirstOrDefault();
                tblProduct product = db.tblProduct.Where(a => a.ID == ProductId).FirstOrDefault();
                if (product.StartPrice < Price && product.Price < Price)
                {
                    if (check != null)
                    {
                        check.Price = Price;
                        product.Price = Price;
                        db.Entry(product).State = System.Data.EntityState.Modified;
                        db.Entry(check).State = System.Data.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        tblUserProduct userP = new tblUserProduct();
                        userP.UserId = UserId;
                        userP.ProductId = ProductId;
                        userP.Price = Price;
                        product.Price = Price;
                        db.Entry(product).State = System.Data.EntityState.Modified;
                        db.tblUserProduct.Add(userP);
                        db.SaveChanges();
                    }
                    response = new { data = 1 };
                }
                else
                {
                    response = new { data = 2 };
                }
                return Json(response);
            }
            catch
            {
                return null;
            }
        }
        public IHttpActionResult GetProduct_All(string TextSearch, string Categorycode)
        {
            try
            {
                object response;
                var para = new DynamicParameters();
                para.Add("@TextSearch", TextSearch);
                para.Add("@Categorycode", Categorycode);

                List<List<dynamic>> lstObject = new List<List<dynamic>>();
                lstObject = ReturnDynamic("GetProduct_All", para);

                response = new { data = lstObject[0] };
                return Json(response);
            }
            catch
            {
                return null;
            }
        }

        public List<List<dynamic>> ReturnDynamic(string procedure, DynamicParameters parameters)
        {
            List<List<dynamic>> lstDynamic = new List<List<dynamic>>();
            //var connection = CimsConstant.SQL_CONNECTION() as SqlConnection;
            //SqlConnection sqlConnection = new SqlConnection("Server=192.168.50.149;initial catalog=Jobbox_Dev;user id=sa;password=123@123a");
            SqlConnection sqlConnection = new SqlConnection("Server=210.245.95.62;Database=T1808m_Group4;User Id=sa;Password=z@GH7ytQ;");
            var command = sqlConnection.CreateCommand();
            if (command.Connection.State != ConnectionState.Open)
            {
                command.Connection.Open();
            }

            var grid = command.Connection.QueryMultiple(sql: procedure,
                                  param: parameters,
                                  commandType: CommandType.StoredProcedure);
            while (!grid.IsConsumed)
            {
                lstDynamic.Add(grid.Read().ToList());
            }
            command.Connection.Close();
            return lstDynamic;
        }
        public IHttpActionResult GetProByid(int proId)
        {
            tblProduct tblProduct = db.tblProduct.Where(p => p.ID == proId).FirstOrDefault();
            return Json(tblProduct);
        }
        public IHttpActionResult GetListUserAution(int proId)
        {
            List<tblUserProduct> tblProduct = db.tblUserProduct.Where(p => p.ProductId == proId).ToList();
            return Json(tblProduct);
        }
        public IHttpActionResult GetListAuction(int UserId)
        {
            ArrayList response = new ArrayList();
            List<tblUserProduct> tblProduct = db.tblUserProduct.Where(p => p.UserId == UserId).ToList();
            foreach (tblUserProduct pro in tblProduct)
            {
                object objectVar;
                tblProduct tblProduc = db.tblProduct.Where(p => p.ID == pro.ProductId).FirstOrDefault();
                objectVar = new {
                    id = pro.ID,
                    productId = pro.ProductId,
                    productName = tblProduc.NameProduct,
                    productCode = tblProduc.CodeProduct,
                    price = pro.Price,
                    type = pro.IsSuccess,
                };
                response.Add(objectVar);
            }
            return Json(response);
        }
        public IHttpActionResult getUserWinner(int price)
        {
            object respponse;
            tblUserProduct tblProduc = db.tblUserProduct.Where(p => p.Price == price).FirstOrDefault();
            if (tblProduc != null)
            {
                tblUsers tblUse = db.tblUsers.Where(p => p.ID == tblProduc.UserId).FirstOrDefault();
                if (tblUse != null)
                {
                    respponse = new { data = tblUse };
                }
                else
                {
                    respponse = new { data = 2 };
                }
            }
            else
            {
                respponse = new { data = 2 };
            }
            return Json(respponse);
        }

    }
}
